import React from 'react';
import './ToolBar.css';
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";

const ToolBar = () => (
    <header className="Toolbar">
            <Logo/>
        <nav>
            <NavigationItems/>
        </nav>
    </header>
);

export default ToolBar;