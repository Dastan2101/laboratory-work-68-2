import React, {Fragment} from 'react';
import ToolBar from "../Navigation/ToolBar/ToolBar";
import './Layout.css';

const Layout = ({children}) => (
        <Fragment>
            <ToolBar/>
            <main className="Layout-Content">
                {children}
            </main>
        </Fragment>
);

export default Layout;