import React from 'react';
import './OrderItem.css';

const OrderItem = props => {

    const ingredientOutput = Object.keys(props.ingredients).map(igKey => (
        <span key={igKey}>{igKey} ({props.ingredients[igKey]})</span>
    ));

    // if (Math.random() > 0.7 ) throw new Error('Well, this is happened');

    return (
        <div className="OrderItem">
            <p>Ingredients: {ingredientOutput}</p>
            <p>Price: {props.price} KGS</p>
        </div>
    );
};

export default OrderItem;